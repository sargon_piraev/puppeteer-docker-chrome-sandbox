const puppeteer = require('puppeteer-core');

(async () => {
    const browser = await puppeteer.connect({ browserURL: process.env.BROWSER_URL });
    const page = await browser.newPage();
    await page.goto(process.env.STORYBOOK_URL);
    await page.screenshot({ path: 'screenshot.png' });
    await browser.disconnect();
})();